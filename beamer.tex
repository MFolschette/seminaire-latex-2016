%%% Présentation de Beamer

\section{Using Beamer}

\begin{frame}[fragile]
  \frametitle{What is Beamer?}

Beamer is a \LaTeX{} class.

\medskip
Common points:
\begin{itemize}
  \item \bemph{structuring} (parts, sections, sub-sections; no chapters),
  \item text formatting,
  \item inclusion of \bemph{figures} and \bemph{mathematical formulas}, etc.
\end{itemize}

\medskip
Main differences:
\begin{itemize}
  \item additional structuring into \bemph{frames},
  \item new commands to handle \bemph{transitions} and animations,
  \item different layout rendering (fonts, arrangement).
\end{itemize}

\medskip
You can reuse your math/figures from your articles from you \LaTeX articles.

Still produces a universally readable PDF (contrary to PPTX or Keynote).
\end{frame}



\begin{frame}[fragile]
  \frametitle{Document definition}

Beamer is a \LaTeX{} class:

\begin{lstlisting}
\documentclass[£\meta{options}£]{beamer}
\end{lstlisting}

\medskip
Amongst the \lstinline?options?:
\begin{itemize}
  \item \lstinline?t?, \lstinline?c? or \lstinline?b? to vertically align the text
    at the top, center or bottom of each frame by default,
  \item \lstinline?Xpt? to set the font size to \lstinline?X? (ex: \lstinline?9pt?),
  \item \lstinline?handout? to produce a printable presentation
    (without transitions/animations).
\end{itemize}

\medskip
Then comes the preamble, then the content, as before inside inside:
\begin{lstlisting}
\begin{document}
  ...
  ...    % The frames come here
  ...
\end{document}
\end{lstlisting}
\end{frame}



\begin{frame}[fragile]
  \frametitle{Definition of a frame}

Each frame comes into an environment called \lstinline?frame?:

\begin{lstlisting}
 \begin{frame}[£\meta{options}£]
   ...
   ...    % Content of the frame
   ...
 \end{frame}
\end{lstlisting}

\medskip
The optional \lstinline?options? may contain:
\begin{itemize}
  \item \lstinline?t?, \lstinline?c? or \lstinline?b?
    to change the vertical alignment for this frame only,
  \item \lstinline?plain? in order not to produce the header and footer
    for this frame only,
  \item \lstinline?shrink? to gain some space if there is a lot of text,
  \item \lstinline?fragile? if the frame contains some source code (like here).
\end{itemize}
\end{frame}



\begin{frame}[fragile]
  \frametitle{Properties of a frame}
  \framesubtitle{Title, subtitle, header, footer, and so on}

A title and a subtitle can be defined for a frame:

\begin{lstlisting}
\frametitle{£\meta{Title of the frame}£}
\framesubtitle{£\meta{Subtitle of the frame}£}
\end{lstlisting}

\bigskip
Furthermore, depending on the theme used,
some information show in the header and footer of each frame:
\begin{itemize}
  \item the current section,
  \item the title of the presentation, the authors and their institute,
  \item the numbering of the frame,
  \item etc.
\end{itemize}
\end{frame}



\begin{frame}[fragile]
  \frametitle{Inside a frame}

The content of a frame is regular \LaTeX, that is:
\begin{itemize}
  \item lists,
  \item figures (containing tables, complex figures, images...),
  \item text and mathematical equations,
  \item etc.
\end{itemize}

\medskip
The content can also be encapsulated into blocks:
\begin{lstlisting}
\begin{exampleblock}{Title of the block}
  Content of the bloc (lists, equations, text...)
\end{exampleblock}
\end{lstlisting}

\begin{exampleblock}{Title of the block}
  Content of the bloc (lists, equations, text...)
\end{exampleblock}

\medskip
By default, 3 kinds of blocks : \lstinline?block?, \lstinline?alertblock? and \lstinline?exampleblock?.
\end{frame}



\begin{frame}[plain]
\begin{figure}
  \centering
  \includegraphics[width=1\textwidth]{img/seance3_extheme_madrid}
\end{figure}
\end{frame}



\section{Animations in Beamer}

\begin{frame}[fragile]
  \frametitle{Animations}

One can define static animations inside Beamer presentations.
\begin{itemize}
  \item<2,4-> They consist in apparitions...
  \item<3-> ...or vanishings.
\end{itemize}

\bigskip
\pause[4]
Animations in fact produce duplicates of the same frame,
where some parts are not printed.
Note that the printed numbering is not affected.

\medskip
The \lstinline?handout? option of the \lstinline?\documentclass?
allows to remove these animations to keep only the final frame.
\end{frame}



\begin{frame}[fragile]
  \frametitle{Successive apparitions}

With command \lstinline?\pause? or \lstinline?\pause[x]?

\medskip
Example with \lstinline?\pause?:

\begin{columns}
  \begin{column}{0.30\textwidth}
\begin{lstlisting}
\begin{itemize}
  \item Bullet 1,
    \pause
  \item Bullet 2,
    \pause
  \item Bullet 3.
\end{itemize}
\end{lstlisting}
  \end{column}
  \begin{column}{0.35\textwidth}
\myex{
\begin{itemize}
  \item Bullet 1,
    \pause
  \item Bullet 2,
    \pause
  \item Bullet 3.
\end{itemize}
}
  \end{column}
\end{columns}
\end{frame}



\begin{frame}[fragile]
  \frametitle{Fine-tuning animations}

Command \lstinline?\only<pages>{content}?: apparition without prior placeholder
\begin{columns}
  \begin{column}{0.50\textwidth}
\begin{lstlisting}
| Text 1,
| 
| \only<2->{Text 2 without placeholder,}
| 
| Text 3.
\end{lstlisting}
  \end{column}
  \begin{column}{0.37\textwidth}
\myex{
Text 1,

\only<2->{Text 2 without placeholder,}

Text 3.
}
  \end{column}
\end{columns}

\bigskip
Command \lstinline?\uncover<pages>{content}?: apparition with prior placeholder
\begin{columns}
  \begin{column}{0.50\textwidth}
\begin{lstlisting}
| Text 1,
| 
| \uncover<3->{Text 2 with placeholder,}
| 
| Text 3.
\end{lstlisting}
  \end{column}
  \begin{column}{0.37\textwidth}
\myex{
Text 1,

\uncover<3->{Text 2 with placeholder,}

Text 3.
}
  \end{column}
\end{columns}

\end{frame}



\begin{frame}[fragile]
  \frametitle{Fine-tuning animations}

Other commands may use an optional \lstinline?<pages>? argument.

\bigskip
Example: \lstinline?\item<pages>?
\begin{columns}
  \begin{column}{0.40\textwidth}
\begin{lstlisting}
\begin{itemize}
  \item<1,2> First bullet
  \item<3-> Second bullet
  \item<2-> Third but not last bullet
\end{itemize}\end{lstlisting}
  \end{column}
  \begin{column}{0.40\textwidth}
\myex{
\begin{itemize}
  \item<1,2> First bullet
  \item<3-> Second bullet
  \item<2-> Third but not last bullet
\end{itemize}
}
  \end{column}
\end{columns}

\bigskip
Another example: most commands of TikZ also accept
the \lstinline?<pages>? syntax.

\end{frame}



\section{Customizing Beamer}

\begin{frame}[fragile]
  \frametitle{Themes}

There exist many predefined themes allowing to modify the layout
and colors of a Beamer presentation.

\bigskip
A \bemph{main theme} is called with \lstinline?\usetheme{theme}? and changes
the layout (title, header, footer, font, shape of the bullets...).

Examples: \lstinline?Warsaw?, \lstinline?Madrid?, \lstinline?Copenhagen?, \lstinline?CambridgeUS?...

\bigskip
A \bemph{color theme} is called with \lstinline?\usecolortheme{theme}?
and only affects the color palette (background, font, blocks, header, footer...).

Examples: \lstinline?beaver?, \lstinline?dolphin?, \lstinline?dove?, \lstinline?fly?...

\bigskip
It is also possible to customize most of these characteristics
and even create a brand new theme.

\bigskip
Refer to the \LaTeX Wikibook \cite{wikibooksbeamer}
for a list of the default themes and customizable parameters.
\end{frame}



\subsection{Examples}

\begin{frame}
  \frametitle{Theme example : CambridgeUS}

\begin{block}{Normal block (neutral)}
  Content of the bloc (lists, equations, text...)
\end{block}

\begin{alertblock}{Alert block}
  If we suppose:
  \begin{equation}
    1+1=0
  \end{equation}
  then we can prove anything.
\end{alertblock}

\begin{exampleblock}{Example block}
  For instance:
  \begin{itemize}
    \item Everything true is also false, and conversely,
    \item $x = y$ for all $x$ and all $y$,
    \item my cat and myself are the same person.
  \end{itemize}
\end{exampleblock}
\end{frame}



\begin{frame}[plain]
\begin{figure}
  \centering
  \includegraphics[width=1\textwidth]{img/seance3_extheme_madrid}
\end{figure}
\end{frame}



\begin{frame}[plain]
\begin{figure}
  \centering
  \includegraphics[width=1\textwidth]{img/seance3_extheme_ecn}
\end{figure}
\end{frame}



\begin{frame}[plain]
\begin{figure}
  \centering
  \includegraphics[width=1\textwidth]{img/seance3_extheme_perso}
\end{figure}
\end{frame}



\subsection{Exercises}

\begin{frame}[fragile]
  \frametitle{Exercise}

Here is the skeleton of a simple presentation:

\begin{lstlisting}[multicols=2]
  \documentclass{beamer}

  \usepackage[french]{babel}
  \usepackage[utf8]{inputenc}

  \usetheme{Madrid}
  \usecolortheme{default}

  \title{Pr£\'e£sentation de ma th£\`e£se}
  \author{Pr£\'e£nom Nom}
  \institute[LDC]{Laboratoire des Chatons}

  \begin{document}

  \begin{frame}
    \maketitle
  \end{frame}

  % A partir d'ici,
  % entrez ce que vous voulez...
  \section{£\`A£ propos de moi}

  \begin{frame}
    \frametitle{Ce que j'aime}
    \begin{itemize}
      \item Les chatons,
      \pause
      \item le jus de raisin,
      \pause
      \item etc.
    \end{itemize}
  \end{frame}

  \end{document}
\end{lstlisting}

\end{frame}
